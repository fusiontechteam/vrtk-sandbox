﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

    //state variables
    public bool introWelcomeAudioPlayed = false; //defines if the introduction audio has played
    public AudioClip introWelcomeAudioClip;

    public bool introHasPlayerMoved = false; //defines if the player has teleported yet
    public AudioClip introPlayerMovementAudioClip;

    public bool introHasPlayerPickedObjectUp = false; //defines if the player has picked up an object
    public AudioClip introPickupObjectAudioClip;

    public AudioSource audioSource;

    public static bool taskInProgress = false;
    public static float elapsedTime; // the time elapsed from the start of the game as a whole
    public static float taskStartTime; // the time at which the player began the task
    public static float taskFinishTime; //the time at which the player finsihed the task
    public static float taskTotalTime; //the time it took the player to go from start to finish
    public static bool taskComplete = false;

    public bool greenDoorLocked = true;

    public GameObject timerObject;

    // Use this for initialization
    void Start () {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = introWelcomeAudioClip;

    }
    
    // Update is called once per frame
    void Update()
    {

        elapsedTime += Time.deltaTime;
        // Debug.Log("Time = "+ elapsedTime);

        if (taskInProgress == true)
        {
            timerObject.GetComponentInChildren<TextMesh>().text = CalculateCurrentTaskTime().ToString();
        }

        //probably change this to a switch statement, but for now if elses will suffice
        if (!audioSource.isPlaying && introWelcomeAudioPlayed == false)
        {

            audioSource.Play();
            introWelcomeAudioPlayed = true;
           
        }
        //TODO: completely change this logic below, it is not correct
        else if (!audioSource.isPlaying && introWelcomeAudioPlayed == true && introHasPlayerMoved == false)
        {
            audioSource.Stop();
            //setup next clip in the intro
            GetComponent<AudioSource>().clip = introPlayerMovementAudioClip;
            audioSource.Play();
            introHasPlayerMoved = true; //TODO: remove this from here and set it externally when player moves

            

        }
        else if (!audioSource.isPlaying && introHasPlayerMoved == true && introHasPlayerPickedObjectUp == false)
        {
            audioSource.Stop();
            //setup next clip in the intro
            GetComponent<AudioSource>().clip = introPickupObjectAudioClip;
            audioSource.Play();
            introHasPlayerPickedObjectUp = true;
        }
    }



    public static void StartTask()
    {
        if (taskInProgress == false && taskComplete == false)
        {
            taskInProgress = true;
            taskStartTime = elapsedTime;
            Debug.Log("Task Started at " + elapsedTime + "seconds");
        }
    }

    public static void FinishTask()
    {
        if (taskInProgress == true)
        {
            taskInProgress = false;
            taskComplete = true;
            taskFinishTime = elapsedTime;
            taskTotalTime = CalculateFinalTaskTime();
            Debug.Log("Task Complete in = " + taskTotalTime + "seconds");
        }
    }

    private float CalculateCurrentTaskTime()
    {
        return elapsedTime - taskStartTime;
    }

    private static float CalculateFinalTaskTime()
    {

        return taskFinishTime - taskStartTime;

    }

    public static bool IsTaskInProgress() {
        return taskInProgress;
    }

}
