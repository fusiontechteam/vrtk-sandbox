﻿/*
Usage: 
1. Attach this script to the object that you wish to emit the audio
2. Ensure object has an Audio Source component attached
3. Associate an audo file with the Audio Source (using its AcudioClip field)
4. Associate the audio with the audioClip variable of this script
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PlayAudio : MonoBehaviour {

	public AudioClip audioClip;    // Add audio clip here;
	AudioSource audioSource;
    public VRTK_InteractableObject interactable;
    public bool playable = true;    //allows us to track whether the audio is in a state we wish to allow it to be played
 

    // Use this for initialization
    void Start () {

		audioSource = GetComponent<AudioSource>();

		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = audioClip;

        VRTK_InteractableObject interactable = gameObject.GetComponent(typeof(VRTK_InteractableObject)) as VRTK_InteractableObject;

        if (interactable != null)
        {
            // Debug.Log("Interactable found ");
        } else {
            // Debug.Log("Interactable NOT found ");
        }
    }
	
	// Update is called once per frame
	void Update () {

        bool touched = interactable.IsGrabbed();
        if (touched)
        {
            //Debug.Log("Interactable IS BEING TOUCHEd ");

            if (!audioSource.isPlaying && playable == true)
            {
                audioSource.Play();
                playable = false;
            }

        } else {

            if (audioSource.isPlaying)
            {
                audioSource.Stop();
                
            }

            playable = true;
        }

        if (interactable != null)
        {
            // Debug.Log("Interactable still here ");
        }
        else
        {
            // Debug.Log("Interactable not here ");
        }

    }

	void OnCollisionEnter (Collision col)  //detects collision has occured
	{

		Debug.Log ("Game object collided with "+col.gameObject.name); 

		if(col.gameObject.name == "Player"){ //put a condition (in this case only if a specific object collides)

			//toggle audio on/off depending on its existing state
			if (!audioSource.isPlaying) {
				audioSource.Play ();
			} else {
				audioSource.Stop ();
			}

		}

	}
}