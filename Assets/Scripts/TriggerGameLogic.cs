﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class TriggerGameLogic : MonoBehaviour
{

    public VRTK_InteractableObject interactable;
    public bool triggersTaskStart = false;
    public bool triggersTaskFinish = false;
    public GameObject objectThatTriggersTaskFinish;

    // Use this for initialization
    void Start()
    {

        VRTK_InteractableObject interactable = gameObject.GetComponent(typeof(VRTK_InteractableObject)) as VRTK_InteractableObject;
        if (interactable != null)
        {
            // Debug.Log("Interactable found ");
        }
        else
        {
            // Debug.Log("Interactable NOT found ");
        }
    }

    // Update is called once per frame
    void Update()
    {

        bool touched = interactable.IsGrabbed();
        if (touched)
        {
            //Debug.Log("Interactable IS BEING TOUCHEd ");

            if (GameLogic.IsTaskInProgress() == false)
            {
                GameLogic.StartTask();
            }

        }


    }

    void OnCollisionEnter(Collision col)  //detects collision has occured
    {

        Debug.Log("Game object collided with " + col.gameObject.name);

        // if this object triggers the task to start
        if (triggersTaskStart)
        {
            // if the player touches the object to start the G
            if (col.gameObject.name == "Player")
            {
                GameLogic.StartTask();
            }
        }
        else if (triggersTaskFinish)
        {
            // if the object colliding with this object is the one we have assigned as the one to trigger the finish
            if (col.gameObject.name == objectThatTriggersTaskFinish.name && GameLogic.IsTaskInProgress())
           {
                GameLogic.FinishTask();
           }
        }
    }
}


